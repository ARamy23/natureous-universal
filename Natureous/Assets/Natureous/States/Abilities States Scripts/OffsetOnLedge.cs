﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Natureous
{
    [CreateAssetMenu(fileName = "New State", menuName = "Natureous/AbilityData/OffsetOnLedge")]
    public class OffsetOnLedge : StateData
    {
        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo animatorStateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);
            animator.SetBool(TransitionParameter.Jump.ToString(), false);
            GameObject anim = control.SkinnedMeshAnimator.gameObject;
            anim.transform.parent = control.LedgeChecker.GrabbedLedge.transform;
            anim.transform.localPosition = control.LedgeChecker.GrabbedLedge.Offset;

            control.Rigidbody.velocity = Vector3.zero;
        }

        public override void OnExit(CharacterState characterStateBase, Animator animator, AnimatorStateInfo animatorStateInfo)
        {

        }

        public override void UpdateAbility(CharacterState characterStateBase, Animator animator, AnimatorStateInfo animatorStateInfo)
        {
            
        }
    }
}


