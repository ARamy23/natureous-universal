﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Natureous
{
    public enum NatureousLayer
    {
        PostProcessing,
        Character,
        Bot,
        Ground,
    }

    public class LayerAdder : MonoBehaviour
    {

    }
}
